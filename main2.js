var name = "";
var email = "";
var lastID = 0;
var lastOnlineNum = 0;
var tempMessageID = 0;
var numOfActivity = 0;

$(document).ready(function(){
	
	var timeout = 3000;
	var isIncrease = false;
	var visitorSelect = $("#visitorNum");
	var visitNum = 0;
	var tempNum = 0;
	
	$("#loginName").focus();
	// update lastID for later use
	// for checking local lastID == DB lastID
	
	// update Visitor number
	if(!sessionStorage["visitcount"]){
		$.post("server.php", {action:"visitors", ip: 1},
			function(data){
				// data.visitors
				sessionStorage["visitcount"] = data.visitors;

				$("#visitorNum").text("Visitors: " + sessionStorage["visitcount"]);
			}, "json");
	}
	else{
		$(".serverInfo").append("Visitors: " + sessionStorage["visitcount"]);
	}
	//animateNumber(0, visitNum, 1500, visitorSelect);
	
	
	$("#loginForm").submit(function(){
		name = $("#loginName").val();
		email = $("#email").val();

		$.post("server2.php", {"action": "login", "name": name, "email": email}, 
			function(data){

				 if(data.loginResult){
					$("#loginForm").fadeOut();
					$("#submitForm").fadeIn();
					$("#message").focus(); 
				 }
				 else{
					 alert("User Name or Email is already in use.");
				 }
			 }, "json");
		
		return false;
	});
	
	$("#submitForm").submit(function(){
		var message = $("#message").val();
		
		$.post("server2.php",
			{"action": "submit", "message": message, "name": name},
			function(data){
				// data.name/message/time_h_i/id
				tempMessageID = data.id;
				appendTempMessage(data);
				},
			"json");
				
		$("#message").val("").focus();
		
		return false;
	});
	
	// when the page close
	$(window).unload(function(){
		logout();
	});
	
	// when the page refresh
	$(window).on('beforeunload', function(){
		logout();
	});
	
	// Self executing timeout functions
	// (continue the looping for requesting server)
	var updateMessages = setInterval(function(){getMessages()}, 1500);

	var updateOnlineUsers = setInterval(function(){getUsers()}, 1000);
	
	/* start bar function */
	
	$(".topBar div").hover(function(){
        $(this).css("background-color", "#E64848");
        }, function(){
        $(this).css("background-color", "#FF7272");
    });
	
	
	//var isMobile = detectMobile();
	if(!isMobile){ // var isMobile from isMobile.js
		//var perfData = window.performance.timing;
		//var connectTime = perfData.responseEnd - perfData.requestStart;
		
		var now = new Date().getTime();
		var CalculationTime = now - performance.timing.navigationStart; // page load time
		CalculationTime.toFixed(4);
		CalculationTime /= 1000;

		$("#topRightDiv").append("<br>Loading Time: " + CalculationTime +"s"
							//"<br>Connection time: " + connectTime + "s" 
							);
		
		//postIP();
	}
	else{
		$("#topRightDiv").append("Mobile "+$(window).width() +"x"+$(window).height());
	}
	/*
	var t1 = setInterval(function(){
		if(isIncrease){
			animateNumber(0, visitNum, 700, visitorSelect);
		}
		else{
			animateNumber(visitNum, 0, 700, visitorSelect);
		}
		isIncrease = !isIncrease;
		
	}, timeout);
	*/
	var t2 = setInterval(function(){
		var date = new Date();
		var dateTime = date.toLocaleTimeString();
		if(isMobile)
			dateTime = dateTime.substr(0);
		else dateTime = dateTime.substr(2);
		$("#time").text(dateTime);
	}, 1000);
});
/*
function postIP(){
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "http://www.telize.com/jsonip?callback=DisplayIP";
        document.getElementsByTagName("head")[0].appendChild(script);
    };
function DisplayIP(response) {
	//document.getElementById("cpuCore").innerHTML = "Your IP Address is " + response.ip;
	$("#cpuCore").append("<br>Your IP Address: " + response.ip);
}
*/
function animateNumber(startNum, endNum, duration, selector){
	jQuery({counter: startNum}).animate({counter: endNum}, {
			duration: duration,
			easing:'swing', // can be anything
			step: function() { // called on every step
				// Update the element's text with rounded-up value:
				selector.text(this.counter.toFixed(2));
			}
	});
}

/*
function appendTempMessage(r){
	var text = '<div id="tempMessageID"> ('+r.time_h_i+') '+
			r.name+': '+
			r.message+'</div><br>';
	var chatLine = $("#chatLineHolder");
	chatLine.append(text);
	chatLine.animate( {scrollTop: chatLine[0].scrollHeight}, 0);
}
*/
function appendMessage(r){ // data.name/message/time/id
	var text = '<div id="text'+r.id+'"> ('+r.time_h_i+') '+
			r.name+': '+
			r.message+'</div><br>';

	var chatLine = $("#chatLineHolder");
	chatLine.append(text);
	chatLine.animate( {scrollTop: chatLine[0].scrollHeight}, 0);
}

function getMessages(){
	$.post("server2.php",
			{"action": "getMessages", "lastID": tempMessageID},  // get all messages above temp and include it
			function(r){
				if(tempMessageID < r.maxid){ // it means there are new message
					//$("#tempMessageID").remove();
					
					tempMessageID = r.maxid;

					for(var i=0; i < r.count; ++i){
						appendMessage(r.chats[i]); // append all new message
					}
				}
				else return;
			}, "json");
}

function getUsers(){
	$.post("server2.php", {action: "getUsers"}, function(data){
		if(lastOnlineNum != data.count){
			
			lastOnlineNum = data.count;
			
			$("#onlineUsers").html("");
			for(var i=0; i<data.count; ++i){
				$("#onlineUsers").append(data.users[i].name+" "+data.users[i].email+"<br>");
			}
			$("#onlineCount").html(data.count+" online");
		}
		else return;
	}, "json");
}	

function logout(){
	$.post("server2.php", {action:"logout", name: name});
}

function getLastID(){
	var id;
	$.post("server2.php",
		   {"action": "getLatestId"},
		   function(r){id = r.lastID;},
		   "json");
	return id;
}